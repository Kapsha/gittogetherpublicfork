# Adding Continuous Integration Pipepline into Existing Code Base

### Description:

#### What is the goal?
In this project, a CI pipeline is implemented on an existing code base of an online dating website. The goal of this project is to learn the basics of CI development. Of course, CI is a development practice usually done by a team of developers; hopefully the fundamentals will still be learned through this activity. 

### Technologies Used
TeamCity by JetBrains will be used to implement automated builds and tests. Bitbucket is used to hold the repository. 

### Results
TBD